#!/usr/bin/env python

from distutils.core import setup

setup(
    name='BrainFuck Compiler',
    version='1.0',
    description='A BrainFuck Compiler and Assembler to machine code',
    author='Quaark',
    url='https://bitbucket.org/quaark/brainfuck-compiler',
    py_modules=['bfcompiler'],
    scripts=['bfc']
)
