# Brain Fuck Compiler

Simply put: This program gets a file with brainfuck code written in it,
compiles it, and either outputs the assembly file or assembles it to a binary.

### Clone or download the repository & install package
Download the repository and run:
```sh
./setup.py install
```

### Usage
Run the command
```sh
bfc -h
```
 And it will output the usage:
```sh
Usage: bfc [options] in_file

Options:
  -h, --help            show this help message and exit
  -S, --assembly        Output assembly file instead of binary file
  -t TAPE_SIZE, --tape-size=TAPE_SIZE
                        Tape size of brainfuck program
  -o OUT_FILE, --output-file=OUT_FILE
                        Name of the output file
```

**Enjoy!**
