
DEFAULT_TAPE_SIZE = 4000


class Compiler(object):
    '''
    Brain Fuck Compiler
    '''

    TEMPLATE = '''
    .section    __TEXT,__text,regular,pure_instructions
    .macosx_version_min 10, 12
    .globl  _main
    .align  4, 0x90

_main:
    .cfi_startproc
    pushq   %rbp
    .cfi_def_cfa_offset 16
    .cfi_offset 6, -16
    movq    %rsp, %rbp
    .cfi_def_cfa_register 6

    leaq    L_.str(%rip), %rdi
    callq   _system
    xorl    %ecx, %ecx

{compiled}

    movl    $0, %eax
    popq    %rbp
    .cfi_def_cfa 7, 8
    ret
    .cfi_endproc

    .comm   _tape,{tape_size},4
    .section    __DATA,__data
    .globl  _i
    .align  3

_i:
    .quad   _tape

    .section    __TEXT,__cstring,cstring_literals
L_.str:
    .asciz  "stty -icanon"

.subsections_via_symbols
    '''

    COMMANDS = {
        '>': ('_normal', [
            '    movq    _i(%rip), %rax',
            '    addq    $1, %rax',
            '    movq    %rax, _i(%rip)',
        ]),
        '<': ('_normal', [
            '    movq    _i(%rip), %rax',
            '    subq    $1, %rax',
            '    movq    %rax, _i(%rip)',
        ]),
        '+': ('_normal', [
            '    movq    _i(%rip), %rax',
            '    movzbl  (%rax), %edx',
            '    addl    $1, %edx',
            '    movb    %dl, (%rax)'
        ]),
        '-': ('_normal', [
            '    movq    _i(%rip), %rax',
            '    movzbl  (%rax), %edx',
            '    subl    $1, %edx',
            '    movb    %dl, (%rax)'
        ]),
        '.': ('_normal', [
            '    movq    _i(%rip), %rax',
            '    movzbl  (%rax), %eax',
            '    movsbl  %al, %eax',
            '    movl    %eax, %edi',
            '    call    _putchar'
        ]),
        ',': ('_putch', [
            '    movq    _i(%rip), %rbx',
            '    call    _getchar',
            '    movb    %al, (%rbx)',
            '    movq    _i(%rip), %rax',
            '    movzbl  (%rax), %eax',
            '    cmpb    $4, %al',
            '    jne     .cond{cond_id}',
            '    movq    _i(%rip), %rax',
            '    movb    $0, (%rax)',
            '.cond{cond_id}:'
        ]),
        '[': ('_loop', [
            '.loop_s{loop_id}:',
            '    movq    _i(%rip), %rax',
            '    movzbl  (%rax), %eax',
            '    cmpb    $0, %al',
            '    je      .loop_e{loop_id}'
        ]),
        ']': ('_end_loop', [
            '   jmp      .loop_s{end_loop_id}',
            '.loop_e{end_loop_id}:'
        ])
    }

    TOKEN_DOC = ' # %s'


    def __init__(self, tokens, tape_size=DEFAULT_TAPE_SIZE):
        '''
        Initializes Class Members
        '''
        self._cond_id = 0
        self._loop_id = 0
        self._loop_stack =[]
        self._tokens = tokens
        self._tape_size = tape_size

    def _normal(self, string):
        '''
        Returns String with no format

        :param str string: string to return
        '''
        return string

    def _putch(self, string):
        '''
        Returns string with formated new Confition ID

        :param str string: string to format and return
        '''
        self._cond_id += 1
        return string.format(cond_id=self._cond_id)

    def _loop(self, string):
        '''
        Returns string with formated new Start Loop ID

        :param str string: string to format and return
        '''
        self._loop_id += 1
        self._loop_stack.append(self._loop_id)
        return string.format(loop_id=self._loop_id)

    def _end_loop(self, string):
        '''
        Returns string with formated new End Loop ID

        :param str string: string to format and return
        '''
        end_loop_id = self._loop_stack.pop()
        return string.format(end_loop_id=end_loop_id)

    def _compile(self):
        '''
        Passes over all tokens(if they are valid) and yields their assembly code
        '''
        for token in self._tokens:
            if token in self.COMMANDS:
                command_lines = [self.TOKEN_DOC % token] + self.COMMANDS[token][1]
                func = getattr(self, self.COMMANDS[token][0])
                yield func('\n'.join(command_lines) + '\n')

    def compile(self):
        '''
        Compiles the code and returns the full assembly code
        '''
        compiled = ''
        for command in self._compile():
            compiled += command
        return self.TEMPLATE.format(compiled=compiled, tape_size=self._tape_size)

